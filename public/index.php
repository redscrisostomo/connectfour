<!DOCTYPE html>
    <html>
    <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script
        src="https://code.jquery.com/jquery-3.6.4.min.js"
        integrity="sha256-oP6HI9z1XaZNBrJURtCoUT5SUnxFr8s3BzRl+cbzUq8="
        crossorigin="anonymous"></script>
    <script src="https://cdn.tailwindcss.com"></script>
    </head>
    <body>
    <div>
        <div class="min-h-screen bg-white">

            <!-- Page Content -->
            <main>
                <div
                    class="relative sm:flex sm:justify-center sm:items-center"
                >
                    <h4 class="text-2xl p-2 tracking-widest">Connect Four Game</h4>
                </div>
                <div class="w-fit  mx-auto p-4 bg-sky-300 ">
                    <div class="flex space-x-8" id="status">
                        <!-- TURNS -->
                        <div class="flex space-x-2">
                            <div>Turns : </div>
                            <div class="flex font-bold space-x-1">
                                <input type="text" class="turns bg-transparent w-8" value=1 />
                            </div>
                        </div>
                        <!-- CURRENT PLAYER -->
                        <div class="flex space-x-2">
                            <div>Current Player : </div>
                            <div class="flex font-bold space-x-1 label-current-player text-red-500">
                                <div>Player</div>
                                <input type="text" class="current_player bg-transparent w-4" value=1 />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="w-fit mx-auto p-4 bg-amber-400 flex shadow-md shadow-amber-900 mt-2">
                    <?php $i=0;?>
                    <?php for($col=0; $col<7;$col++) : // setup columns ?>
                    <div class="flex-col space-y-2 drow border-4 border-amber-400 hover:border-sky-300">
                            <?php for($row=0; $row<6;$row++) : //setup rows?>
                                <div class="bg-white w-16 h-16 rounded-full dcol empty" data-row="<?=$row?>" data-col="<?=$col?>" data-id="<?=(($row)*7)+$col?>" id="div_<?=$row?>_<?=$col?>"></div>
                                <?php $i++ ?>
                            <?php endfor; ?>
                        </div>
                    <?php endfor; ?>
                </div>
                <div class="w-fit mx-auto mt-4">
                    <a href="javascript:;" class="bg-sky-700 p-2 rounded text-white" onclick="refresh();">Reset</a>
                </div>
                <input type="hidden" class="player1" /> <!-- storage of player 1 moves -->
                <input type="hidden" class="player2" /> <!-- storage of player 2 moves -->
            </main>
        </div>
    </div>
    <script>
        var isComplete = false; //indicator if game is complete
        var isTied = false; //indicator if game is a draw or tied
        //set default values
        $("document").ready(function(){
            $(".turns").val(1);
            $(".current_player").val(1);
            $(".player1").val("");
            $(".player2").val("");
        });
        //method for reset button
        function refresh()
        {
            window.location.reload();
        }
        /**set process when slot is clicked */
        $(".dcol").each(function(){
            //when each round slot is clicked
            $(this).on('click', function(){
                if(isComplete | isTied) //if already complete if tied, just do nothing
                {
                    return;
                }
                var current_player = $(".current_player").val();
                var turns = $(".turns").val();
                //get the last empty row of the parent column of clicked slot
                var lastEmpty = getLastEmpty($(this).closest('.drow'));
                var color = ""; //designated bgcolor
                if(current_player==1)
                {
                    color="red";
                    $(".current_player").val(2);
                }
                else
                {
                    color="indigo";
                    $(".current_player").val(1);
                }
                //colorize the last empty row
                lastEmpty.addClass("bg-"+color+"-500").removeClass("empty").removeClass("bg-white");
                //colorize the current player label
                $(".label-current-player").removeClass("text-"+color+"-500").addClass("text-"+(color=='red' ? 'indigo' : 'red')+"-500"); 
                //add the slot id to the player moves storage
                setPlayerIds(current_player,lastEmpty.data('id'));
                //check if game is complete
                checkComplete(current_player);
                turns = parseInt(turns)+1; //increment the turn
                if(isComplete) //if complete, throw message
                {
                    $("#status").html("Game Complete! Player "+current_player+" wins!");
                    $(".drow").removeClass("hover:border-sky-300");
                }
                else if(turns==43) //if no moves available, set as tied and throw message
                {
                    isTied = true;
                    if(isTied)
                    {
                        $("#status").html("Game Tied!");
                        $(".drow").removeClass("hover:border-sky-300");
                    }
                }
                else
                {
                    $(".turns").val(turns); //update current no. of turns
                }
            });
        });
        /**Method to get the last empty row of selected column */
        function getLastEmpty(elem)
        {
            var children = elem.children(".empty:last");
            return children;
            // console.log(children);
        }
        /**Method to add the id of the slot to the player moves */
        function setPlayerIds(player,id)
        {
            //get the stored player moves
            var val = $(".player"+player).val().length> 0 ?  $(".player"+player).val() : "";
            //convert into array
            var values = val.length>0 ? val.split(",") : [];
            //push the slot id to the array
            values.push(id);
            //update the player storage
            $(".player"+player).val(values.join(","));
        }

        /**Method to check if the game is complete */
        function checkComplete(player)
        {
            /**the winning combination of slot ids */
            var winningArrays = [
                [0, 1, 2, 3],
                [41, 40, 39, 38],
                [7, 8, 9, 10],
                [34, 33, 32, 31],
                [14, 15, 16, 17],
                [27, 26, 25, 24],
                [21, 22, 23, 24],
                [20, 19, 18, 17],
                [28, 29, 30, 31],
                [13, 12, 11, 10],
                [35, 36, 37, 38],
                [6, 5, 4, 3],
                [0, 7, 14, 21],
                [41, 34, 27, 20],
                [1, 8, 15, 22],
                [40, 33, 26, 19],
                [2, 9, 16, 23],
                [39, 32, 25, 18],
                [3, 10, 17, 24],
                [38, 31, 24, 17],
                [4, 11, 18, 25],
                [37, 30, 23, 16],
                [5, 12, 19, 26],
                [36, 29, 22, 15],
                [6, 13, 20, 27],
                [35, 28, 21, 14],
                [0, 8, 16, 24],
                [41, 33, 25, 17],
                [7, 15, 23, 31],
                [34, 26, 18, 10],
                [14, 22, 30, 38],
                [27, 19, 11, 3],
                [35, 29, 23, 17],
                [6, 12, 18, 24],
                [28, 22, 16, 10],
                [13, 19, 25, 31],
                [21, 15, 9, 3],
                [20, 26, 32, 38],
                [36, 30, 24, 18],
                [5, 11, 17, 23],
                [37, 31, 25, 19],
                [4, 10, 16, 22],
                [2, 10, 18, 26],
                [39, 31, 23, 15],
                [1, 9, 17, 25],
                [40, 32, 24, 16],
                [9, 17, 25, 33],
                [8, 16, 24, 32],
                [11, 17, 23, 29],
                [12, 18, 24, 30],
                [1, 2, 3, 4],
                [5, 4, 3, 2],
                [8, 9, 10, 11],
                [12, 11, 10, 9],
                [15, 16, 17, 18],
                [19, 18, 17, 16],
                [22, 23, 24, 25],
                [26, 25, 24, 23],
                [29, 30, 31, 32],
                [33, 32, 31, 30],
                [36, 37, 38, 39],
                [40, 39, 38, 37],
                [7, 14, 21, 28],
                [8, 15, 22, 29],
                [9, 16, 23, 30],
                [10, 17, 24, 31],
                [11, 18, 25, 32],
                [12, 19, 26, 33],
                [13, 20, 27, 34],
            ];
            /**get the player moves */
            var val = $(".player"+player).val().length> 0 ?  $(".player"+player).val() : "";
            /**convert into array */
            var values = val.length>0 ? val.split(",") : [];
            /**loop through each winning combinations */
            for (let i = 0; i < winningArrays.length; ++i) {
                var square1 = winningArrays[i][0];
                var square2 = winningArrays[i][1];
                var square3 = winningArrays[i][2];
                var square4 = winningArrays[i][3];
                var $count = 0;
                /**loop though the player moves */
                for(let v = 0; v < values.length; ++v)
                {
                    /**if 1st value of winning combination is matched */
                    if(parseInt(square1) == parseInt(values[v])) 
                    {
                        $count++;
                    }
                    /**if 2nd value of winning combination is matched */
                    if(parseInt(square2) == parseInt(values[v]))
                    {
                        $count++;
                    }
                    /**if 3rd value of winning combination is matched */
                    if(parseInt(square3) == parseInt(values[v]))
                    {
                        $count++;
                    }
                    /**if 4th value of winning combination is matched */
                    if(parseInt(square4) == parseInt(values[v]))
                    {
                        $count++;
                    }
                }
                /**if the counter has reached 4, then the game is complete */
                if($count >= 4)
                {
                    isComplete = true;
                }
            }
        }
    </script>
    </body>
</html>